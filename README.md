<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Things todo list

1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/multi-step-form-example-in-laravel.git`
2. Navigate to the folder: `cd multi-step-form-example-in-laravel`
3. Run the command: `composer install`
4. Run the command: `copy env.example .env`
5. Set base on your MySQL Credentials
6. Run the command: `php artisan generate:key`
7. Open your favorite browser: http://localhost:8000/products

### Images Screenshot

List All Product

![List All Product](img/list1.png "List All Product")

![List All Product](img/list2.png "List All Product")

Step by Step

![Step by Step](img/step1.png "Step by Step")

![Step by Step](img/step2.png "Step by Step")

![Step by Step](img/step3.png "Step by Step")
